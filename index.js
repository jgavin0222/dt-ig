var fs=require('fs');
var input=process.argv;
input.shift();
input.shift();
var db="./database.txt";
var rules="./rules.txt";
var rulesJson="./rules.json";
var generate=inputParse(input,"g");
var learn=inputParse(input,"l");
var predict=inputParse(input,"p");
var deleteDB=inputParse(input,"d");
if(predict){
	try{
		if(checkExist(rules)){
			try{
				var predictSet = inputParse(input,"-data");
				var rules = fs.readFileSync(rulesJson,'utf8');
				var parsed = JSON.parse(predictSet);
				var rulesParsed = JSON.parse(rules);
				var first = Object.keys(rulesParsed)[0];
				function predictor(key,parsed,rulesParsed,identifier){
					if(!key){
						var key = Object.keys(rulesParsed)[0];
						return predictor(true,parsed,rulesParsed[key],key)
					}else{
						var sigRule = rulesParsed[parsed[identifier]];
						if(sigRule=='true' || sigRule=='false'){
							return sigRule;
						}else{
							return predictor(false,parsed,rulesParsed[parsed[identifier]]);
						}
					}
				}
				console.log(predictor(false,parsed,rulesParsed,first))
			}catch(err){
				console.log(err);
				console.log("An error occured finding prediction data");
			}
		}else{
			console.log("Rules file is not present, please generate the rules file");
		}
	}catch(err){
		console.log(err);
		console.log("An error occured");
	}
}else if(generate){
	try{
		if(checkExist(rules)){
			try{
				console.log("Rules file found");
				fs.unlinkSync(rules);
				console.log("Rules file deleted");
			}catch(err){
				console.log(err);
				console.log("Error deleting the rules file");
			}
		}
		if(checkExist(rulesJson)){
			try{
				console.log("Rules JSON file found");
				fs.unlinkSync(rulesJson);
				console.log("Rules JSON file deleted");
			}catch(err){
				console.log(err);
				console.log("Error deleting the rules JSON file");
			}
		}
		function getEntropy(right,wrong){
			var ratioA=right/(right+wrong);
			var ratioB=wrong/(right+wrong);
			var leftLog=Math.log2(ratioA);
			var rightLog=Math.log2(ratioB);
			var left=ratioA*leftLog;
			var right=ratioB*rightLog;
			var total=-left-right||0;
			return total.toFixed(2);
		}
		function getGain(right,wrong,obj){
			var ent=getEntropy(right,wrong);
			var keys=Object.keys(obj);
			total=0;
			for(let i=0;i<keys.length;i++){
				var yes=parseFloat(obj[keys[i]].yes);
				var no=parseFloat(obj[keys[i]].no);
				total+=yes+no;
			}
			for(let i=0;i<keys.length;i++){
				var yes=parseFloat(obj[keys[i]].yes);
				var no=parseFloat(obj[keys[i]].no);
				var ratio=(yes+no)/total;
				var partialEnt=getEntropy(yes,no);
				ent-=(ratio*partialEnt);
			}
			return ent;
		}
		if(checkExist(db)){
			fs.readFile(db,(err,data)=>{
				if(!err){
					var bigEntArray=[];
					var array=JSON.parse(data);
					var trueArray=array.filter((data)=>data["r"]=="true");
					var falseArray=array.filter((data)=>data["r"]=="false");
					var header=Object.keys(array[0]);
					header.pop();
					var zero=0;
					var removeWhat="";
					var advanceTree={};
					function getBestHeader(header,array){
						for(let i=0;i<header.length;i++){
							var arrayUnique=[];
							var fullData={};
							var uniqueData=array.map((data,iteration)=>{
								var found=false;
								for(let j=0;j<arrayUnique.length;j++){
									if(data[header[i]]==arrayUnique[j]){
										var found=true;
									}
								}
								if(!found){
									arrayUnique.push(data[header[i]])
								}
							});
							arrayUnique.map((AUD,AUI)=>{
								var no=0;
								var yes=0;
								array.map((AUDX,AUDXI)=>{
									if(AUD==AUDX[header[i]]){
										if(AUDX["r"]=="true"){
											yes++;
										}else{
											no++;
										}
									}
								});
								fullData[AUD]={yes:yes,no:no}
							});
							var gain=getGain(trueArray.length,falseArray.length,fullData);
							if(gain>zero){
								zero=gain;
								removeWhat=header[i];
								advanceTree=fullData;
							}
						}
						header.splice(header.indexOf(removeWhat),1);
						bigEntArray.push(removeWhat);
						try{
							fs.appendFileSync(rules,"Main : branch: "+removeWhat+"\n");
						}catch(err){
							console.log(err);
							console.log("An error occured when writing rules");
						}
						return {node:removeWhat,remaining:header,tree:advanceTree,array:array};
					}
					function getBestBranch(split){
						var node=split.node;
						var tree=split.tree;
						var remaining=split.remaining;
						var array=split.array;
						var uniqueArray=Object.keys(tree);
						for(let i=0;i<uniqueArray.length;i++){
							var zero=0;
							var header="leaf";
							var outside={};
							var wasNo=false;
							var wasYes=false;
							var branch=uniqueArray[i];
							var avail=array.filter((original)=>{return original[node]==branch});
							var yes=tree[uniqueArray[i]].yes;
							var no=tree[uniqueArray[i]].no;
							for(let j=0;j<remaining.length;j++){
								var dArray=[];
								var fullData={}
								avail.map((AUD)=>{
									var found=false;
									for(let k=0;k<dArray.length;k++){
										if(dArray[k]==AUD[remaining[j]]){
											found=true;
										}
									}
									if(!found){
										dArray.push(AUD[remaining[j]]);
									}
								});
								dArray.map((dData)=>{
									var iYes=avail.filter((aFilter)=>{return aFilter[remaining[j]]==dData}).filter((aFilter)=>{return aFilter["r"]=="true"});
									var iNo=avail.filter((aFilter)=>{return aFilter[remaining[j]]==dData}).filter((aFilter)=>{return aFilter["r"]=="false"});
									fullData[dData]={yes:iYes.length,no:iNo.length}
								});
								var gain=getGain(yes,no,fullData);
								if(gain>zero){
									zero=gain;
									header=remaining[j];
									outside=fullData;
								}else if(zero==0){
									header="leaf";
								}
							}
							if(header!="leaf"){
								//console.log(node+" : "+uniqueArray[i]+" : branch: "+header);
								try{
									fs.appendFileSync(rules,node+" : "+uniqueArray[i]+" : branch: "+header+"\n");
								}catch(err){
									console.log(err);
									console.log("An error occured when writing rules");
								}
								remaining.splice(remaining.indexOf(header),1);
								getBestBranch({node:header,remaining:remaining,tree:outside,array:avail})
							}else{
								if(getEntropy(tree[uniqueArray[i]].yes,tree[uniqueArray[i]].no)==0){
									var isTrue="true";
									if(tree[uniqueArray[i]].yes<tree[uniqueArray[i]].no){
										isTrue="false";
									}else if(tree[uniqueArray[i]].yes==tree[uniqueArray[i]].no){
										isTrue="toss up";
									}
									try{
										fs.appendFileSync(rules,node+" : "+uniqueArray[i]+" : results "+isTrue+"\n");
									}catch(err){
										console.log(err);
										console.log("An error occured when writing rules");
									}
									//console.log("Leaf: "+node+" value: "+uniqueArray[i]+" : results "+isTrue);
								}
							}
						}
					}
					var initalSplit=getBestHeader(header,array);
					getBestBranch(initalSplit);
					if(checkExist(rules)){
						var rulesObject = {};
						var rulesData=fs.readFileSync(rules,"utf8").split('\n').filter(Boolean);
						var main=rulesData[0].split("branch:")[1].trim();
						fs.appendFileSync(rulesJson,JSON.stringify(writePredictionData(rulesData,[],main,rulesObject)));
					}
					console.log("Algorithm building complete");
				}else{
					console.log("Failed to read database");
					console.log(err);
				}
			});
		}else{
			console.log("Cannot predict, database does not exist")
		}
	}catch(err){
		console.log("An error occured");
	}
}else if(learn){
	console.log("Learning");
	if(checkExist(db)){
		try{
			console.log("DB Exists");
			fs.readFile(db,(err,data)=>{
				if(!err){
					learnData(JSON.parse(data));
				}else{
					console.log("Failed to read existing DB");
				}
			});
		}catch(err){
			console.log("Failed to learn addition data");
			console.log(err);
		}
	}else{
		try{
			learnData([]);
			console.log("DB Created Successfully");
		}catch(err){
			console.log("Failed to create DB");
			console.log(err);
		}
	}
	function validateData(data){
		switch(data){
			case "True":{
				return true;
			}
			case "False":{
				return false;
			}
			default:{
				return data;
			}
		}
	}
	function learnData(array){
		var data=inputParse(input,"-data");
		array.push(JSON.parse(data));
		fs.writeFile(db,JSON.stringify(array),(err)=>{
			if(!err){
				console.log("Success");
			}else{
				console.log(err);
			}
		});
	}
}else if(deleteDB){
	console.log("Deleting");
	fs.unlink(db,(err)=>{
		if(!err){
			console.log("Success");
		}else{
			console.log("Failed to delete DB");
			console.log(err);
		}
	});
}
function checkExist(file){
	return fs.existsSync(file);
}
function getRandomNum(min,max){
  return Math.round(Math.random() * (max - min) + min);
}

function inputParse(input,search){
	var found=false;
	for(let i=0;i<input.length;i++){
		var data=input[i].split("=");
		if(data[0]=="-"+search){
			found=true;
			return data[1] || true;
		}
	}
	if(!found){
		return false;
	}
}

function writePredictionData(rules,path,nextPath,bundle){
	path.push(nextPath);
	for(var i=1;i<rules.length;i++){
		if(rules[i].split(":")[0].trim()==nextPath){
			if(rules[i].indexOf("results")!=-1){
				var tmpPath = path.slice(0);
				tmpPath.push(rules[i].split(":")[1].trim());
				bundle = verifyWritable(tmpPath,bundle,rules[i].split("results")[1].trim());
			}else{
				var tmpPath = path.slice(0);
				tmpPath.push(rules[i].split(":")[1].trim());
				bundle = writePredictionData(rules,tmpPath,rules[i].split("branch:")[1].trim(),bundle);
			}
		}
	}
	return bundle;
}

function verifyWritable(fields,object,writeData,lastObj){
	if(lastObj === undefined){lastObj=object;}
  	var fieldsLength = fields.length;
    if(fieldsLength === 1 && writeData!==undefined){
    	if(fields[0] !== "*array*"){
      		lastObj[fields[0]] = writeData;
    	}else{
     		lastObj[0] = writeData;
    	}
    	return lastObj;
    }
    if(fieldsLength > 0){
  		var firstField = fields[0];
    	var secondField = fields[1];
    	fields.splice(0,1);
    	if(lastObj[firstField]===undefined){
    		if(secondField === "*array*"){
        		lastObj[firstField] = [];
    		}else if(firstField === "*array*"){
	        	if(lastObj[0] === undefined){
	        		lastObj[0] = {};
	        	}
	        	lastObj[0] = verifyWritable(fields,object,writeData,lastObj[0]);
	        	return lastObj;
	       }else{
	    	lastObj[firstField] = {};
	       }
     	}
  		lastObj[firstField] = verifyWritable(fields,object,writeData,lastObj[firstField]);
  		return lastObj;
  	}
}
